package uk.co.nickthecoder.tockle.resources

import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.WriterConfig
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter

class ResourceWriter(val file: File) {

    fun write(root: Root) {
        val jRoot = JsonObject()

        writeChildren(jRoot, root)

        BufferedWriter(OutputStreamWriter(FileOutputStream(file))).use { writer ->
            jRoot.writeTo(writer, WriterConfig.PRETTY_PRINT)
        }
    }

    private fun writeChildren(jParent: JsonObject, parent: ParentResource) {
        if (parent.children.isNotEmpty()) {
            val jChildren = JsonObject()
            jParent.add("children", jChildren)

            for (child in parent.children) {
                val jObject = JsonObject()
                jChildren.add(child.name, jObject)
                writeResource(jObject, child)
            }
        }
    }

    private fun writeResource(jObject: JsonObject, resource: Resource) {
        println("writeResource ${resource.type} $resource ")
        jObject.add("type", resource.type)
        when (resource) {
            is GameInfo -> writeGameInfo(jObject, resource)
            is Texture -> writeTextureInfo(jObject, resource)
            is Sprite -> writeSpriteInfo(jObject, resource)
        }
        if (resource is ParentResource) {
            writeChildren(jObject, resource)
        }
    }

    private fun writeGameInfo(jObject: JsonObject, resource: GameInfo) {
        jObject.add("title", resource.title)
        jObject.add("width", resource.width)
        jObject.add("height", resource.height)
    }

    private fun writeTextureInfo(jObject: JsonObject, resource: Texture) {
        jObject.add("file", resource.file.path)
    }

    private fun writeSpriteInfo(jObject: JsonObject, resource: Sprite) {
        jObject.add("left", resource.rect.left)
        jObject.add("top", resource.rect.top)
        jObject.add("width", resource.rect.width)
        jObject.add("height", resource.rect.height)
    }

}

