package uk.co.nickthecoder.tockle.resources

import uk.co.nickthecoder.tockle.util.PixelRect
import java.io.File

/**
 * The [ResourcesReader] is used by the editor, and when just playing the game.
 * However, the editor allows the resources to be edited, and they make heavy use of JavaFX Properties.
 * We don't want the Tockle runtime to depend on JavaFX, therefore when editing a game, we use a
 * different set of classes to store the game data.
 *
 * This factory returns either "Simple" classes, which cannot be changed, or "Editable" classes, which
 * use JavaFX properties (in the tockle-editor sub-project).
 */
interface ResourceFactory {

    fun createRootNode(file: File): Root

    fun createGameInfo(
        rootNode: Root,

        title: String,
        width: Int,
        height: Int
    ): GameInfo

    fun createTextureInfo(
        parentNode: ParentResource,
        name: String,

        file: File
    ): Texture

    fun createSpriteInfo(
        parentNode: Texture,
        name: String,

        rect: PixelRect
    ): Sprite

}

/**
 * Simple resources cannot be edited. These are used when playing a game, but not when
 * using the editor.
 */
class SimpleResourceFactory : ResourceFactory {

    override fun createRootNode(file: File): Root = SimpleRoot(file)

    override fun createGameInfo(
        rootNode: Root,

        title: String,
        width: Int,
        height: Int

    ): GameInfo = SimpleGameInfo(
        rootNode,

        title = title,
        width = width,
        height = height
    )

    override fun createTextureInfo(
        parentNode: ParentResource,
        name: String,
        file: File
    ): Texture = SimpleTexture(parentNode, name, file)

    override fun createSpriteInfo(
        parentNode: Texture,
        name: String,
        rect: PixelRect
    ): Sprite = SimpleSprite(parentNode, name, rect)

}
