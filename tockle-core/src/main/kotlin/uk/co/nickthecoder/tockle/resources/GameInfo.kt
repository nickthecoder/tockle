package uk.co.nickthecoder.tockle.resources

interface GameInfo : ChildResource {

    override val type
        get() = "gameInfo"

    val title: String
    val width: Int
    val height: Int

    override fun text() = "${super.text()} : Size [$width,$height]"
}

internal class SimpleGameInfo(

    rootNode: Root,

    override val title: String,
    override val width: Int,
    override val height: Int

) : SimpleLeafResource(

    rootNode,
    "GameInfo"

), GameInfo {
}
