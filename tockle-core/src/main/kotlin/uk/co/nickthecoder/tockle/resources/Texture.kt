package uk.co.nickthecoder.tockle.resources

import java.io.File

interface Texture : ParentResource, ChildResource, FileResource {

    override val type: String
        get() = "texture"

    override fun text() = super<ChildResource>.text() + " : $file"

}

internal class SimpleTexture(
    parentNode: ParentResource,
    name: String,
    override val file: File
) : SimpleParentResource(parentNode, name), Texture {

}
