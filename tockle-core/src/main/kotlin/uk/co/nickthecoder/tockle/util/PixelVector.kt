package uk.co.nickthecoder.tockle.util

/**
 * Most coordinates in the game, use floating point numbers, not integers.
 * The exception to this is when dealing with pixels, such as stating the position and size of a Sprite
 * within a Texture image.
 *
 * There is another subtle difference when using pixel coordinates... The origin of a Texture is at the
 * top left, with the Y axis pointing downwards.
 * The other coordinate systems tend to but the origin in the bottom left, with the Y axis pointing upwards.
 */
data class PixelVector(val top: Int, val right: Int)
