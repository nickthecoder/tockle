package uk.co.nickthecoder.tockle.resources

import java.io.File


interface Root : Resource, ParentResource, FileResource {

    override val type
        get() = "Root"

    override val parent: ParentResource?
        get() = null

    override fun findRoot() = this

    fun gameInfo() = find("/Game Info") as GameInfo

    override fun path() = "/"

    fun findDependants(path: String): List<Resource> {
        val result = mutableListOf<Resource>()

        fun findDependants(resource: Resource) {
            if (resource.path() != path) {
                if (resource.dependsOn(path)) {
                    result.add(resource)
                } else {
                    if (resource is ParentResource) {
                        for (child in resource.children) {
                            findDependants(child)
                        }
                    }
                }
            }
        }
        findDependants(this)

        return result
    }

    fun dump() {
        fun dump(indent: String, resource: Resource) {

            print(indent)
            println(resource)
            if (resource is ParentResource) {
                val nextIndent = indent + "    "
                for (child in resource.children) {
                    dump(nextIndent, child)
                }
            }
        }
        dump("", this)
    }
}

internal class SimpleRoot(override val file: File) : Root {

    override val name: String
        get() = ""

    override val children = mutableListOf<ChildResource>()

    override fun toString() = path()

}
