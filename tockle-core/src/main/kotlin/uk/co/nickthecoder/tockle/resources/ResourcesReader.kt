package uk.co.nickthecoder.tockle.resources

import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.JsonValue
import uk.co.nickthecoder.tockle.util.PixelRect
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

class ResourcesReader(
    private val file: File,
    private val resourceFactory: ResourceFactory
) {

    private val rootNode = resourceFactory.createRootNode(file)

    /**
     * Keeps track of where we a reading. Used for error messages.
     */
    private var path = ""

    fun read(): Root {
        val jRoot: JsonObject = Json.parse(InputStreamReader(FileInputStream(file))).asObject()
        readChildren(jRoot, rootNode)

        if (rootNode.child("Game Info") == null) {
            rootNode.children.add(
                resourceFactory.createGameInfo(
                    rootNode,
                    title = "<Untitled Game>",
                    width = 800, height = 600
                )
            )
        }

        return rootNode
    }

    private fun toObject(jValue: JsonValue): JsonObject {
        return jValue.asObject()
    }

    private fun getInt(jObject: JsonObject, name: String): Int {
        val value = jObject.get(name)
        return value.asInt()
    }

    private fun getFile(jObject: JsonObject): File {
        val value = jObject.get("file")
        return File(value.asString())
    }

    private fun readChildren(jParent: JsonObject, parent: ParentResource) {
        val jChildren = jParent.get("children") as? JsonObject
        if (jChildren != null) {

            val oldPath = path
            jChildren.forEach { jMember: JsonObject.Member ->
                val name = jMember.name
                val type = (jMember.value as? JsonObject)?.getString("type", null)
                if (type != null) {
                    path = "$oldPath/${name}"
                    val jChild = toObject(jMember.value)
                    val child: Resource? = when (type) {
                        "gameInfo" -> readGameInfo(jChild)
                        "texture" -> readTexture(jChild, parent, name)
                        "sprite" -> readSprite(jChild, parent, name)
                        else -> null
                    }
                    if (child is ParentResource) {
                        readChildren(jChild, child)
                    }
                }
            }
            path = oldPath
        }
    }

    private fun readGameInfo(jObject: JsonObject): GameInfo {
        val title = jObject.getString("title", "My Game")
        val width = jObject.getInt("width", 800)
        val height = jObject.getInt("height", 600)

        val gameInfo = resourceFactory.createGameInfo(
            rootNode,

            title = title,
            width = width,
            height = height
        )

        rootNode.children.add(gameInfo)
        return gameInfo
    }

    private fun readTexture(jObject: JsonObject, parent: ParentResource, name: String): Texture {
        val file = getFile(jObject)

        val textureInfo = resourceFactory.createTextureInfo(
            parent,
            name,
            file
        )

        parent.children.add(textureInfo)
        return textureInfo
    }

    private fun readSprite(jObject: JsonObject, parent: ParentResource, name: String): Sprite? {
        val textureInfo = (parent as? Texture) ?: return null

        val left = getInt(jObject, "left")
        val top = getInt(jObject, "top")
        val width = getInt(jObject, "width")
        val height = getInt(jObject, "height")

        val spriteInfo = resourceFactory.createSpriteInfo(
            textureInfo,
            name,
            PixelRect(top, left, width, height)
        )

        parent.children.add(spriteInfo)
        return spriteInfo
    }
}
