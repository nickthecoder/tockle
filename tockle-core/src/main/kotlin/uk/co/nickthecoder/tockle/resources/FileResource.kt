package uk.co.nickthecoder.tockle.resources

import java.io.File

interface FileResource {
    val file : File
}
