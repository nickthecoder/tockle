package uk.co.nickthecoder.tockle.util

/**
 * Like [PixelVector], the origin of a PixelRect is in the top left corner.
 * i.e. the Y axis points downwards.
 */
data class PixelRect(
    val left: Int,
    val top: Int,
    val width: Int,
    val height: Int
) {
    val right: Int
        get() = left + width

    val bottom: Int
        get() = top + height
}
