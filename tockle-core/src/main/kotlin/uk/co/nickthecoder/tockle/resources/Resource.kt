package uk.co.nickthecoder.tockle.resources

interface Resource {

    val type: String
    val name: String
    val parent: ParentResource?

    fun findRoot(): Root
    fun path(): String

    /**
     * This should be the same as [toString].
     * The only reason for it being here, is that I cannot define [toString] on interfaces,
     * and I do not want to re-implement it on each concrete implementation of each resource type.
     */
    fun text(): String = "${path()} ($type)"

    fun dependsOn( path : String ) : Boolean = false

}

interface ParentResource : Resource {

    val children: MutableList<ChildResource>

    fun find(name: String): Resource? {
        return if (name.startsWith("/")) {
            findRoot().find(name.substring(1))
        } else {
            val slash = name.indexOf("/")
            if (slash >= 0) {
                val childName = name.substring(0, slash)
                val child = child(childName)
                if (child is ParentResource) {
                    child.find(name.substring(slash + 1))
                } else {
                    null
                }
            } else {
                child(name)
            }
        }
    }

    // NOTE, we could improve runtime performance by overriding this method, and using a hashmap
    // keyed on the child names.
    fun child(name: String): ChildResource? = children.firstOrNull { it.name == name }
}


interface ChildResource : Resource {

    override val parent: ParentResource

    override fun findRoot() = parent.findRoot()

    override fun path(): String {
        val parentPath = parent.path()
        return if (parentPath == "/") {
            "/$name"
        } else {
            "$parentPath/$name"
        }
    }
}


internal abstract class SimpleParentResource(
    override val parent: ParentResource,
    override val name: String
) : ChildResource, ParentResource {

    override val children = mutableListOf<ChildResource>()
    override fun toString() = text()
}


internal abstract class SimpleLeafResource(
    override val parent: ParentResource,
    override val name: String
) : ChildResource {

    override fun toString() = text()

}
