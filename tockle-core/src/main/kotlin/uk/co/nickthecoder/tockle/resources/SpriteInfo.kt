package uk.co.nickthecoder.tockle.resources

import uk.co.nickthecoder.tockle.util.PixelRect

interface Sprite : ParentResource, ChildResource {

    override val type
        get() = "sprite"

    val texture: Texture
        get() = parent as Texture

    val rect: PixelRect

    override fun text() = super<ChildResource>.text() + " : $rect"

}

internal open class SimpleSprite(
    parent: Texture,
    name: String,
    override val rect: PixelRect

) : SimpleParentResource(parent, name), Sprite {

}
