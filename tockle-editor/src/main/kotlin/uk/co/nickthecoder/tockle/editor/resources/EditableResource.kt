package uk.co.nickthecoder.tockle.editor.resources

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import uk.co.nickthecoder.tockle.editor.util.PropertyValue
import uk.co.nickthecoder.tockle.resources.ChildResource
import uk.co.nickthecoder.tockle.resources.ParentResource
import uk.co.nickthecoder.tockle.resources.Resource
import java.io.File

interface EditableResource : Resource {

    fun directory(): File

    val nameProperty: StringProperty
    override var name: String
}

interface EditableParentResource : EditableResource, ParentResource {
    override val children: ObservableList<ChildResource>
}

interface EditableChildResource : EditableResource, ChildResource

/**
 * An [EditableResource], which is NOT an [EditableRoot], but is a [ParentResource],
 * therefore it has a parent, and children.
 */
abstract class EditableRegularResource(
    override val parent: EditableParentResource,
    name: String
) : EditableParentResource, EditableChildResource {

    final override val nameProperty: StringProperty = SimpleStringProperty(name)
    override var name: String by PropertyValue(nameProperty)

    override val children: ObservableList<ChildResource> = FXCollections.observableArrayList()

    override fun directory(): File = parent.directory()

    override fun toString() = text()

}

abstract class EditableLeafResource(
    override val parent: EditableParentResource,
    name: String
) : EditableResource, ChildResource {

    final override val nameProperty: StringProperty = SimpleStringProperty(name)
    override var name: String by PropertyValue(nameProperty)

    override fun directory() = parent.directory()

    override fun toString() = text()

}

