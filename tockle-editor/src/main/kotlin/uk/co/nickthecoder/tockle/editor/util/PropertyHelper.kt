package uk.co.nickthecoder.tockle.editor.util

import javafx.beans.property.*
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class PropertyValue<T>(private val fxProperty: Property<T>)
    : ReadWriteProperty<Any, T> {

    override fun getValue(thisRef: Any, property: KProperty<*>): T = fxProperty.value

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        fxProperty.value = value
    }
}

class IntegerPropertyValue(private val fxProperty: IntegerProperty)
    : ReadWriteProperty<Any, Int> {

    override fun getValue(thisRef: Any, property: KProperty<*>): Int = fxProperty.value

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Int) {
        fxProperty.value = value
    }
}

class DoublePropertyValue(private val fxProperty: DoubleProperty)
    : ReadWriteProperty<Any, Double> {

    override fun getValue(thisRef: Any, property: KProperty<*>): Double = fxProperty.value

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Double) {
        fxProperty.value = value
    }
}
