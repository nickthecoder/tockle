package uk.co.nickthecoder.tockle.editor.dockables

import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.Image
import uk.co.nickthecoder.fxessentials.ApplicationAction
import uk.co.nickthecoder.harbourfx.Dockable

abstract class TockleDockable(applicationAction: ApplicationAction) : Dockable {

    override val dockableIcon: Image? = applicationAction.image

    override val dockableTitle = SimpleStringProperty(applicationAction.label)

    override val dockablePrefix = SimpleStringProperty("")

    init {
        applicationAction.shortcutNumber()?.let {
            dockablePrefix.set("${it}:")
        }
    }

}
