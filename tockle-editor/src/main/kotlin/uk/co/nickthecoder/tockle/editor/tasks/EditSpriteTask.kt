package uk.co.nickthecoder.tockle.editor.tasks

import uk.co.nickthecoder.tockle.editor.resources.EditableResourceFactory
import uk.co.nickthecoder.tockle.editor.resources.EditableSprite
import uk.co.nickthecoder.tockle.editor.resources.EditableTexture
import uk.co.nickthecoder.tockle.editor.util.PixelVectorParameter
import uk.co.nickthecoder.tockle.util.PixelRect

open class EditSpriteTask(

    parent: EditableTexture,
    resource: EditableSprite?

) : AbstractEditResourceTask<EditableSprite>("sprite", parent, resource) {

    constructor(resource: EditableSprite) : this(resource.parent as EditableTexture, resource)

    val positionP = PixelVectorParameter("position")
    val sizeP = PixelVectorParameter("size", 32, 32)

    init {
        parameters.addAll(positionP, sizeP)
    }

    override fun run() {
        super.run()
        resource?.let { resource ->
            resource.rect = PixelRect(positionP.xP.value!!, positionP.yP.value!!, sizeP.xP.value!!, sizeP.yP.value!!)
        }
    }

}


class NewSpriteTask(

    parent: EditableTexture

) : EditSpriteTask(parent, null) {

    override fun run() {

        if (resource == null) {
            val newSprite = EditableResourceFactory().createSpriteInfo(
                parent as EditableTexture,
                nameP.value,
                PixelRect(positionP.xP.value!!, positionP.yP.value!!, sizeP.xP.value!!, sizeP.yP.value!!)
            )
            resource = newSprite
            parent.children.add(newSprite)
        }
        super.run()
    }

}
