package uk.co.nickthecoder.tockle.editor.resources

import javafx.beans.property.Property
import javafx.beans.property.SimpleObjectProperty
import uk.co.nickthecoder.tockle.editor.util.PropertyValue
import uk.co.nickthecoder.tockle.resources.Texture
import java.io.File

class EditableTexture(
    parentNode: EditableParentResource,
    name: String,
    file: File
) : EditableRegularResource(parentNode, name), Texture {

    val fileProperty: Property<File> = SimpleObjectProperty(file)
    override var file by PropertyValue(fileProperty)

}
