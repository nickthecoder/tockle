package uk.co.nickthecoder.tockle.editor.dockables

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.input.MouseEvent
import javafx.util.Callback
import uk.co.nickthecoder.tockle.editor.EditorActions
import uk.co.nickthecoder.tockle.editor.EditorGUI
import uk.co.nickthecoder.tockle.editor.resources.*
import uk.co.nickthecoder.tockle.editor.tasks.NewSpriteTask
import uk.co.nickthecoder.tockle.editor.tasks.NewTextureTask
import uk.co.nickthecoder.tockle.editor.tasks.deleteResource
import uk.co.nickthecoder.tockle.resources.*

class ResourcesTreeDockable : TockleDockable(EditorActions.DOCKABLE_RESOURCES) {

    override val dockableId: String
        get() = id

    override val dockableContent: ObjectProperty<Node> = SimpleObjectProperty(ResourcesTree())

    companion object {
        val id = "resourcesTree"
    }
}

class ResourcesTree() : TreeView<EditableResource>(createItem(Resources.instance as EditableRoot)) {

    init {
        isShowRoot = false
        cellFactory = Callback { ResourceTreeCell() }
    }

    companion object {
        private fun createItem(resource: EditableResource): ResourceTreeItem {
            val result = ResourceTreeItem(resource)

            if (resource is ParentResource) {
                resource.children.forEach { child ->
                    if (child is EditableResource) {
                        result.children.add(createItem(child))
                    }
                }
            }

            return result
        }
    }

    class ResourceTreeItem(val resource: EditableResource) : TreeItem<EditableResource>(resource) {

        val childListener = ListChangeListener<ChildResource> { change ->
            while (change.next()) {
                println("Children of $resource changed : Add? ${change.wasAdded()} Delete ${change.wasRemoved()}")
                if (change.wasRemoved()) {
                    val removed: List<ChildResource> = change.removed
                    children.removeIf { childItem ->
                        val childResource: ChildResource? = (childItem as ResourceTreeItem).resource as? ChildResource
                        if (removed.contains(childResource)) println("Removing $removed from the tree")
                        removed.contains(childResource)
                    }
                }
                if (change.wasAdded()) {
                    for (child in change.addedSubList) {
                        if (child is EditableResource) {
                            val item = createItem(child)
                            addChild(item)
                        }
                    }
                }

            }
        }

        init {
            if (resource is EditableParentResource) {
                resource.children.addListener(childListener)
            }
        }

        private fun addChild(item: ResourceTreeItem) {
            val name = item.resource.name
            children.forEachIndexed { index, treeItem ->
                val otherItem = treeItem as ResourceTreeItem
                if (otherItem.resource.name > name) {
                    children.add(index, item)
                    return
                }
            }
            // Fell off the end without finding a bigger one. Add it to the end.
            children.add(item)
        }
    }

    inner class ResourceTreeCell : TreeCell<EditableResource>() {

        var resource: EditableResource? = null

        init {
            onMousePressed = EventHandler { event -> onMouse(event) }
            onMouseReleased = EventHandler { event -> onMouse(event) }
            onMouseClicked = EventHandler { event -> onMouseClicked(event) }
        }


        override fun updateItem(item: EditableResource?, empty: Boolean) {
            super.updateItem(item, empty)

            resource = item
            if (empty || item == null) {
                textProperty().unbind()
                text = ""
                graphic = null
            } else {
                textProperty().bind(item.nameProperty)
                graphic = null
            }
        }

        private fun editResource() {
            resource?.let { resource ->
                EditorGUI.instance?.editResource(resource)
                if (resource is EditableParentResource) {
                    EditorGUI.instance?.editResource(resource)
                }
            }
        }

        private fun onMouse(event: MouseEvent) {
            if (event.isPopupTrigger) {
                onPopup(event)
                event.consume()
            }


        }

        private fun onMouseClicked(event: MouseEvent) {
            if (event.clickCount == 2) {
                event.consume()
                editResource()
            }
        }

        private fun onPopup(event: MouseEvent) {
            val res: EditableResource = resource ?: Resources.instance as EditableRoot

            val menu = ContextMenu()
            if (res is EditableParentResource) {
                menu.items.addAll(createAddMenuItems(res))
            }

            if (res !is GameInfo && res is EditableChildResource) {
                menu.items.add(createDeleteMenuItem(res))
            }

            menu.show(this@ResourcesTree.scene.window, event.screenX, event.screenY)
        }

        private fun createAddMenuItems(res: EditableParentResource): List<MenuItem> {
            val result = mutableListOf<MenuItem>()

            result.add(MenuItem("Add Texture").apply {
                onAction = EventHandler {
                    NewTextureTask(res).prompt()
                }
            })

            if (res is EditableTexture) {
                result.add(MenuItem("Add Sprite").apply {
                    onAction = EventHandler {
                        NewSpriteTask(res).prompt()
                    }
                })
            }

            return result
        }

        private fun createDeleteMenuItem(res: EditableChildResource): MenuItem {
            return MenuItem("Delete ${res.type} '${res.name}'").apply {
                onAction = EventHandler {
                    deleteResource(res)
                }
            }
        }

    }

}
