package uk.co.nickthecoder.tockle.editor.resources

import javafx.beans.property.Property
import javafx.beans.property.SimpleObjectProperty
import uk.co.nickthecoder.tockle.editor.util.PropertyValue
import uk.co.nickthecoder.tockle.util.PixelRect
import uk.co.nickthecoder.tockle.resources.Sprite

class EditableSprite(
    parent: EditableTexture,
    name: String,
    rect: PixelRect
) : EditableRegularResource(parent, name), Sprite {

    val rectProperty: Property<PixelRect> = SimpleObjectProperty(rect)
    override var rect: PixelRect by PropertyValue(rectProperty)

}
