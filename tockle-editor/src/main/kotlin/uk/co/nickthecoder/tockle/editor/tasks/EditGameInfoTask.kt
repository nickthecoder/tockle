package uk.co.nickthecoder.tockle.editor.tasks

import uk.co.nickthecoder.paratask.AbstractParameterizedTask
import uk.co.nickthecoder.paratask.parameters.StringParameter
import uk.co.nickthecoder.tockle.editor.resources.EditableGameInfo
import uk.co.nickthecoder.tockle.editor.util.PixelVectorParameter
import uk.co.nickthecoder.tockle.resources.Resources


class EditGameInfoTask : AbstractParameterizedTask("gameInfo"), EditResourceTask {

    override val resource = Resources.instance.gameInfo() as EditableGameInfo
    val gameInfo = resource

    private val titleP = StringParameter("title", gameInfo.title)
    private val sizeP = PixelVectorParameter("size", gameInfo.width, gameInfo.height)

    init {
        parameters.addAll(titleP, sizeP)
    }

    override fun run() {
        gameInfo.title = titleP.value
        gameInfo.width = sizeP.xP.value!!
        gameInfo.height = sizeP.yP.value!!
    }

}
