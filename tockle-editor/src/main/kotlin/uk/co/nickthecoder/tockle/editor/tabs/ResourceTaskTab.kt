package uk.co.nickthecoder.tockle.editor.tabs

import javafx.event.EventHandler
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.layout.BorderPane
import uk.co.nickthecoder.paratask.gui.TaskForm
import uk.co.nickthecoder.tockle.editor.resources.EditableResource
import uk.co.nickthecoder.tockle.editor.tasks.EditResourceTask

class ResourceTaskTab(val task: EditResourceTask) : Tab(), ResourceTab {

    override val resource: EditableResource
        get() = task.resource!!

    private val form = TaskForm(task).apply {
        onDone = EventHandler { close() }
    }

    private val borderPane = BorderPane().apply {
        top = Label(task.label).apply {
            styleClass.add("tabTitle")
        }
        center = form
    }

    init {
        textProperty().bind(task.resource!!.nameProperty)
        content = borderPane
    }

    private fun close() {
        tabPane?.tabs?.remove(this)
    }
}
