package uk.co.nickthecoder.tockle.editor.tasks

import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import uk.co.nickthecoder.tockle.editor.EditorGUI
import uk.co.nickthecoder.tockle.editor.resources.EditableChildResource

fun deleteResource(resource: EditableChildResource) {

    val result = Alert(Alert.AlertType.CONFIRMATION).apply {
        title = "Delete ${resource.type} ${resource.name}?"
        headerText = title
        contentText = "You cannot undo this!"
    }.showAndWait()

    if (result.isPresent && result.get() == ButtonType.OK) {
        var dependants = resource.findRoot().findDependants(resource.path())
        if (dependants.size > 20) {
            dependants = dependants.subList(0, 20)
        }
        if (dependants.isNotEmpty()) {
            Alert(Alert.AlertType.WARNING).apply {
                title = "Cannot Delete ${resource.type} ${resource.name}"
                headerText = "The resource '${resource.name}' cannot be deleted,\ndue to the following dependants :"
                contentText = dependants.joinToString(separator = "\n") { it.path() }
            }.showAndWait()
        } else {
            EditorGUI.instance?.closeResourceTab(resource)
            resource.parent.children.remove(resource)
        }
    }
}
