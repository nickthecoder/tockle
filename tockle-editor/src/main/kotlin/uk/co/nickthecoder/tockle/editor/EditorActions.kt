package uk.co.nickthecoder.tockle.editor

import javafx.scene.input.KeyCode
import uk.co.nickthecoder.fxessentials.AllActions

object EditorActions {

    private val actions = AllActions(EditorApp::class.java)


    //val NEW = actions.create("new", "New", KeyCode.EQUALS, control = true, tooltip = "Create a New Resource")
    val RUN = actions.create("run", "Run", KeyCode.R, control = true, tooltip = "Run the game")
    val TEST = actions.create("test", "Test", KeyCode.T, control = true, shift = true, tooltip = "Run the Test Scene")

    //val RELOAD = actions.create("reload", "Reload", KeyCode.F5, tooltip = "Reload textures, fonts & sounds")
    //val OPEN = actions.create("open", "Open", KeyCode.O, control = true, tooltip = "Open Resource")
    val SAVE = actions.create("save", "Save", KeyCode.S, control = true)

    val DOCKABLE_RESOURCES = actions.create("dockable.resources", "Resources", KeyCode.DIGIT1, alt = true)

}
