package uk.co.nickthecoder.tockle.editor.tabs

import uk.co.nickthecoder.tockle.editor.resources.EditableResource

interface ResourceTab {
    val resource: EditableResource
}
