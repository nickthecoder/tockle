package uk.co.nickthecoder.tockle.editor.tasks

import uk.co.nickthecoder.paratask.parameters.FileParameter
import uk.co.nickthecoder.tockle.editor.resources.EditableParentResource
import uk.co.nickthecoder.tockle.editor.resources.EditableResourceFactory
import uk.co.nickthecoder.tockle.editor.resources.EditableTexture
import java.io.File

open class EditTextureTask(

    parent: EditableParentResource,
    resource: EditableTexture?

) : AbstractEditResourceTask<EditableTexture>("texture", parent, resource) {

    constructor(resource: EditableTexture) : this(resource.parent, resource)

    val fileP = FileParameter("file", resource?.file ?: File("")).apply {
        required = true
        mustExist = true
        expectFile = true
        extensions.add("png")
    }

    init {
        parameters.addAll(fileP)
    }

    override fun run() {
        super.run()
        resource?.let { resource ->
            // TODO Handle renaming of the file rename.
            resource.file = fileP.value
        }
    }

}


class NewTextureTask(

    parent: EditableParentResource

) : EditTextureTask(parent, null) {

    override fun run() {
        if (resource == null) {
            // Do we need to copy the file?
            val parentDirectory = parent.directory().absoluteFile
            val absFile = fileP.value.absoluteFile
            val fileDirectory = absFile.parentFile
            val destFile = File(parentDirectory, absFile.name)
            if (fileDirectory != fileDirectory) {
                absFile.copyTo(destFile)
            }

            val textureInfo = EditableResourceFactory().createTextureInfo(
                parent,
                nameP.value,
                destFile
            )
            resource = textureInfo
            parent.children.add(textureInfo)

        }
        super.run()

    }

}

