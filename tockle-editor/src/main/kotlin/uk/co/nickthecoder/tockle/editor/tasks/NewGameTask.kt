package uk.co.nickthecoder.tockle.editor.tasks

import uk.co.nickthecoder.paratask.AbstractParameterizedTask
import uk.co.nickthecoder.paratask.parameters.FileParameter
import uk.co.nickthecoder.paratask.parameters.IntegerParameter
import uk.co.nickthecoder.paratask.parameters.StringParameter
import uk.co.nickthecoder.tockle.editor.EditorApp
import uk.co.nickthecoder.tockle.editor.resources.EditableResourceFactory
import uk.co.nickthecoder.tockle.resources.ResourceWriter
import java.io.File

class NewGameTask : AbstractParameterizedTask("New Game") {

    val fileP = FileParameter("file", File("newGame.tockle")).apply {
        mustExist = false
        expectFile = true
        extensions.add("tockle")
    }

    val titleP = StringParameter("title")
    private val widthP = IntegerParameter("width", 800)
    private val heightP = IntegerParameter("height", 600)

    init {
        parameters.addAll(fileP, titleP, widthP, heightP)
    }

    override fun run() {
        val factory = EditableResourceFactory()
        val file = fileP.value
        val root = factory.createRootNode(file)
        val gameInfo = factory.createGameInfo(
            root,
            titleP.value,
            widthP.value!!, heightP.value!!
        )
        root.children.add(gameInfo)
        ResourceWriter(file).write(root)

        EditorApp.showGUI(file)
    }

}
