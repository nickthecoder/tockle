package uk.co.nickthecoder.tockle.editor.resources

import uk.co.nickthecoder.tockle.resources.*
import uk.co.nickthecoder.tockle.util.PixelRect
import java.io.File

class EditableResourceFactory : ResourceFactory {

    override fun createRootNode(file: File) =
        EditableRoot(file)

    override fun createGameInfo(
        rootNode: Root,

        title: String,
        width: Int,
        height: Int
    ) =
        EditableGameInfo(
            rootNode as EditableRoot,

            title = title,
            width = width,
            height = height
        )

    override fun createTextureInfo(parentNode: ParentResource, name: String, file: File) =
        EditableTexture(
            parentNode as EditableParentResource,
            name,
            file
        )

    override fun createSpriteInfo(parentNode: Texture, name: String, rect: PixelRect) =
        EditableSprite(
            parentNode as EditableTexture,
            name,
            rect
        )

}
