package uk.co.nickthecoder.tockle.editor.util

import uk.co.nickthecoder.paratask.parameters.GroupParameter
import uk.co.nickthecoder.paratask.parameters.IntegerParameter

class PixelVectorParameter(name: String, x: Int = 0, y: Int = 0) : GroupParameter(name) {

    val xP = IntegerParameter("${name}X", x).apply { suffix = " ," }
    val yP = IntegerParameter("${name}Y", y)

    init {
        parameters.addAll(xP, yP)
        this.horizontally()
    }

}
