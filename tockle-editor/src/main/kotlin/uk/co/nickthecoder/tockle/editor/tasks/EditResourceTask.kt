package uk.co.nickthecoder.tockle.editor.tasks

import uk.co.nickthecoder.paratask.AbstractParameterizedTask
import uk.co.nickthecoder.paratask.ParameterizedTask
import uk.co.nickthecoder.paratask.parameters.Parameter
import uk.co.nickthecoder.paratask.parameters.StringParameter
import uk.co.nickthecoder.tockle.editor.resources.EditableChildResource
import uk.co.nickthecoder.tockle.editor.resources.EditableParentResource
import uk.co.nickthecoder.tockle.editor.resources.EditableResource

interface EditResourceTask : ParameterizedTask {
    val resource: EditableResource?
}

abstract class AbstractEditResourceTask<R : EditableChildResource>(

    name: String,
    val parent: EditableParentResource,
    final override var resource: R?

) : AbstractParameterizedTask(name), EditResourceTask {

    val nameP = StringParameter("name", resource?.name ?: "")

    init {
        parameters.add(nameP)
    }

    override fun check(): Parameter? {
        var result = super<AbstractParameterizedTask>.check()
        for (sibling in parent.children) {
            if (sibling !== resource && sibling.name == nameP.value) {
                if (result == null) result = nameP
            }
        }
        return result
    }

    override fun run() {
        resource?.let { resource ->
            if (resource.name != nameP.value) {
                resource.name = nameP.value
                // By removing and re-adding the resource, the ResourceTree can update itself.
                resource.parent.children.remove(resource)
                resource.parent.children.add(resource)
            }
        }
    }

}
