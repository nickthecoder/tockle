package uk.co.nickthecoder.tockle.editor.resources

import javafx.beans.property.Property
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import uk.co.nickthecoder.tockle.editor.util.PropertyValue
import uk.co.nickthecoder.tockle.resources.ChildResource
import uk.co.nickthecoder.tockle.resources.ParentResource
import uk.co.nickthecoder.tockle.resources.Root
import java.io.File

class EditableRoot(file: File) : EditableParentResource, Root {

    override val nameProperty: StringProperty = SimpleStringProperty("")
    override var name: String by PropertyValue(nameProperty)

    val fileProperty: Property<File> = SimpleObjectProperty(file)
    override val file: File by PropertyValue(fileProperty)

    override val children: ObservableList<ChildResource> = FXCollections.observableArrayList()

    override fun toString() = file.nameWithoutExtension

    override fun directory(): File = file.parentFile

}
