package uk.co.nickthecoder.tockle.editor

import javafx.application.Application
import javafx.stage.Stage
import uk.co.nickthecoder.tockle.editor.resources.EditableResourceFactory
import uk.co.nickthecoder.tockle.editor.tasks.NewGameTask
import uk.co.nickthecoder.tockle.resources.Resources
import uk.co.nickthecoder.tockle.resources.ResourcesReader
import java.io.File
import java.util.prefs.Preferences

class EditorApp : Application() {

    override fun start(stage: Stage) {

        val parameters = parameters
        val unnamed = parameters.unnamed
        val filename: String? = if (unnamed.size == 1) unnamed[0] else null

        if (filename == null) {

            NewGameTask().prompt()

        } else {
            val file = File(filename)
            showGUI(file, stage)
        }
    }


    override fun stop() {
        EditorGUI.instance?.onStopApplication()
        super.stop()
    }

    companion object {
        val preferences = Preferences.userNodeForPackage(EditorApp::class.java)

        internal fun showGUI(file: File, stage: Stage? = null) {
            val resourceFactory = EditableResourceFactory()

            val s = stage ?: Stage()
            Resources.instance = ResourcesReader(file, resourceFactory).read()
            Resources.instance.dump()

            EditorGUI(s)
            s.show()
        }
    }
}
