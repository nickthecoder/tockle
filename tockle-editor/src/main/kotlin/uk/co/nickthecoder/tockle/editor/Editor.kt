package uk.co.nickthecoder.tockle.editor

import javafx.application.Application

class Editor {
    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Application.launch(EditorApp::class.java, * args)
        }
    }
}
