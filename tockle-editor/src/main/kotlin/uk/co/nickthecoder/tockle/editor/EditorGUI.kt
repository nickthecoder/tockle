package uk.co.nickthecoder.tockle.editor

import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.stage.Stage
import uk.co.nickthecoder.fxessentials.ActionGroup
import uk.co.nickthecoder.fxessentials.addFxEssentialsCSS
import uk.co.nickthecoder.harbourfx.Dockable
import uk.co.nickthecoder.harbourfx.DockableFactory
import uk.co.nickthecoder.harbourfx.Harbour
import uk.co.nickthecoder.paratask.ParaTask
import uk.co.nickthecoder.tockle.editor.dockables.ResourcesTreeDockable
import uk.co.nickthecoder.tockle.editor.resources.EditableGameInfo
import uk.co.nickthecoder.tockle.editor.resources.EditableResource
import uk.co.nickthecoder.tockle.editor.resources.EditableSprite
import uk.co.nickthecoder.tockle.editor.resources.EditableTexture
import uk.co.nickthecoder.tockle.editor.tabs.*
import uk.co.nickthecoder.tockle.editor.tasks.EditGameInfoTask
import uk.co.nickthecoder.tockle.editor.tasks.EditSpriteTask
import uk.co.nickthecoder.tockle.editor.tasks.EditTextureTask
import uk.co.nickthecoder.tockle.resources.ResourceWriter
import uk.co.nickthecoder.tockle.resources.Resources

class EditorGUI(stage: Stage) : DockableFactory {

    private val actionGroup = ActionGroup().apply {
        action(EditorActions.SAVE) { onSave() }
        action(EditorActions.DOCKABLE_RESOURCES) { harbour.show(ResourcesTreeDockable.id) }
    }

    private val menu = MenuBar().apply {
        menus.add(Menu("File").apply {
            items.add(actionGroup.menuItem(EditorActions.SAVE))
        })
        menus.add(Menu("View").apply {
            items.add(actionGroup.menuItem(EditorActions.DOCKABLE_RESOURCES))
        })
    }

    private val toolBar = ToolBar().apply {
        items.add(actionGroup.button(EditorActions.SAVE))
    }

    private val menuAndToolbars = VBox().apply {
        children.addAll(menu, toolBar)
    }

    private val tabPane = TabPane().apply {
        tabs.add(ResourceTaskTab(EditGameInfoTask()))
    }

    private val harbour = Harbour(tabPane, this)

    private val borderPane = BorderPane().apply {
        top = menuAndToolbars
        center = harbour
    }

    private val scene = Scene(borderPane, 800.0, 800.0)


    init {
        scene.stylesheets.add(ParaTask.cssUrl)
        scene.stylesheets.add(Harbour.cssUrl)
        scene.addFxEssentialsCSS()

        stage.title = "Tockle Editor"
        stage.scene = scene
        instance = this

        if (!harbour.load(EditorApp.preferences)) {
            harbour.left.mainHalf.dockables.add(ResourcesTreeDockable())
        }
    }

    internal fun onStopApplication() {
        harbour.save(EditorApp.preferences)
    }

    override fun createDockable(dockableId: String): Dockable? {
        return when (dockableId) {
            ResourcesTreeDockable.id -> ResourcesTreeDockable()
            else -> null
        }
    }

    /**
     * Closes any tabs that are editing the [resource].
     * Any changes will be lost. Used when deleting a resource from the ResourcesTree
     */
    fun closeResourceTab(resource: EditableResource) {
        tabPane.tabs.removeIf {
            it is ResourceTaskTab && it.resource === resource
        }
    }

    fun editResource(resource: EditableResource) {
        for (tab in tabPane.tabs) {
            if (tab is ResourceTab) {
                if (tab.resource.path() == resource.path()) {
                    tabPane.selectionModel.select(tab)
                    return
                }
            }
        }

        // We didn't find an existing tab with that resource, so let's add one.
        val tab: Tab? = when (resource) {
            is EditableGameInfo -> ResourceTaskTab(EditGameInfoTask())
            is EditableTexture -> ResourceTaskTab(EditTextureTask(resource))
            is EditableSprite -> ResourceTaskTab(EditSpriteTask(resource))
            else -> {
                println("Unexpected resource : $resource")
                null
            }
        }

        tab?.let {
            tabPane.tabs.add(it)
            tabPane.selectionModel.select(tab)
        }

    }

    fun onSave() {
        Resources.instance.dump()
        ResourceWriter(Resources.instance.file).write(Resources.instance)
    }

    companion object {
        var instance: EditorGUI? = null
    }
}
