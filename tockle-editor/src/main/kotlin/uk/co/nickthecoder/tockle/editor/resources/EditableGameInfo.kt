package uk.co.nickthecoder.tockle.editor.resources

import uk.co.nickthecoder.tockle.resources.GameInfo
import uk.co.nickthecoder.tockle.resources.Root

class EditableGameInfo(

    rootNode: EditableRoot,
    override var title: String,
    override var width: Int,
    override var height: Int

) : EditableLeafResource(rootNode, "Game Info"), GameInfo {

}
